namespace ParallelPort
{
	using System.Collections;
	using UnityEngine;
	using UnityEngine.InputSystem;

    public class ParallelPortCommunication : MonoBehaviour
    {
		[SerializeField]
		ushort _portAddress;

		private static readonly string PLAYER_PREFS_KEY_NAME = "ParallelPortAddress";

		public ushort portAddress
		{
			get => _portAddress;
			set
			{
				_portAddress = value;
				SaveAddress();
			}
		}

		#region Unity callbacks

		void Start()
        {
			bool isX64 = InpOutInterface.IsXP64Bit_x64();

            if (!isX64)
            {
				Debug.LogError("Not a x64 architecture, deactivating gameobject.");
				gameObject.SetActive(false);
            }

			LoadAddress();
        }

        #endregion

        #region Public methods
            
        public short InputFromLPT()
        {
			return InpOutInterface.Input_x64(_portAddress);
		}

        public void OutputToLPT(short value)
        {
			InpOutInterface.Output_x64(_portAddress, value);
        }

		#endregion

		#region Saving/Loading parallel address

		public void SaveAddress()
		{
			PlayerPrefs.SetInt(PLAYER_PREFS_KEY_NAME, _portAddress);
		}

		public void LoadAddress()
		{
			_portAddress = (ushort)PlayerPrefs.GetInt(PLAYER_PREFS_KEY_NAME, _portAddress);
		}

		#endregion
	}
}
