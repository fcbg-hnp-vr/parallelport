namespace ParallelPort
{
    using System;
	using System.Runtime.InteropServices;
	using System.Collections.Generic;

    /// <summary>
    /// Static class to communicate with the Inputx64 dll.
    /// </summary>
    /// <remarks>
    /// Only support inpoutx64 for now.
    /// </remarks>
	public static class InpOutInterface
    {
		// public const string path32 = "inpout32.dll";
		public const string dllName_x64 = "inpoutx64.dll";

		#region Input / Output

		[DllImport(dllName_x64, EntryPoint = "Out32")]
		public static extern void Output_x64(ushort portAddress, short data);

		[DllImport(dllName_x64, EntryPoint = "Inp32")]
		public static extern short Input_x64(ushort portAddress);

        #endregion

        #region Extra usefull functions

		[DllImport(dllName_x64, EntryPoint = "IsInpOutDriverOpen")]
		public static extern UInt32 IsInpOutDriverOpen_x64();

        [DllImport(dllName_x64, EntryPoint = "IsXP64Bit")]
		public static extern bool IsXP64Bit_x64();

		#endregion

		#region DLLPortIO function support

		// Note: No uchar in C#, need to check what could fit here.
		// [DllImport(dllName_x64, EntryPoint = "DlPortReadPortUchar")]
		// public static extern uchar DlPortReadPortUchar_x64(short portAddress);
		// [DllImport(dllName_x64, EntryPoint = "DlPortWritePortUchar")]
		// public static extern void DlPortWritePortUchar_x64(short portAddress, uchar data);

		[DllImport(dllName_x64, EntryPoint = "DlPortReadPortUshort")]
		public static extern ushort DlPortReadPortUshort_x64(ushort portAddress);
		[DllImport(dllName_x64, EntryPoint = "DlPortWritePortUshort")]
		public static extern void DlPortWritePortUshort_x64(ushort portAddress, ushort data);

		[DllImport(dllName_x64, EntryPoint = "DlPortReadPortUlong")]
		public static extern ulong DlPortReadPortUlong_x64(ulong portAddress);
		[DllImport(dllName_x64, EntryPoint = "DlPortWritePortUlong")]
		public static extern void DlPortWritePortUlong_x64(ulong portAddress, ulong data);

		#endregion
    }
}
