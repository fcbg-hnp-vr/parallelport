# Parallel Port

A package that provide Parallel Port scripts and library for use with Unity.

## Requirements

To be able to use that package and communicate with a LPT port, you need :
 - A parallel port device
 - A x64 windows computer


## Installation

**The steps below are required to make this package work !**

- This package can be added to Unity via git url with `https://gitlab.com/fcbg-hnp-vr/parallelport.git`.

- You then need to install the driver, using the [`InstallDriver.exe`](./Runtime/InpOutBinaries_1501/Win32/InstallDriver.exe). It has to be launched from the same folder as the inpout32.dll. Don't forget that your final user will also have to install that (separately or you can also include it inside an installer).

- Find the Parallel Port address :
    - Type System Information in the windows search bar. Open it.
    - Go to Hardware Resources, then I/O.
    - Find the LPT device. The first value in Resource is the Hex address.
    - You can then enter the Hex address directly in Unity (starting with 0x), or convert it to 10-base before. (Example: 0x00002FE8 => 12264).


## License

This package is released under the Mozilla Public License, see [LICENSE.md](./LICENSE.md).  
That package use the InpOut dll to communicate with the LPT port, provided under the MIT License (see [license.txt](./Runtime/InpOutBinaries_1501/license.txt))
