# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2022-05-11
### Fixed
- Build creation (remove inpout32.h in x64 folder)
- Missing meta files
- Fix var type (ushort) for port address 


## [1.0.1] - 2022-05-02
### Fixed
- InstallDriver issues without inpout32.dll.


## [1.0.0] - 2022-04-19
First release of this package.
